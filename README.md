### SLUG Bayesian Modelling of LEGUS Cluster Demographics Pipeline (2022)

## Dependencies 

1. Install [slug source code](https://bitbucket.org/krumholz/slug2/src/master/) before you can use any of the analytical Python tools in this directory. 

2. Include Dan Foreman-Mackey's [emcee package](https://github.com/dfm/emcee).

3. Git pull the cluster library, dowmloadable from INSERT HYPERLINK OF CLUSTER_SLUG LIBRARY HERE. 

## Repository Workflow
The key steps are:
1. Download the cluster catalog file(s) from [MAST]....LINK HERE. We have 

## Layout 

## License 

This code distributed under the GNU 3.0 license. 